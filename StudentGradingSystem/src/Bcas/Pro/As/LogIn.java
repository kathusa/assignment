package Bcas.Pro.As;

/**
 * Login system for the report card.
 * 
 * @author KathuSha
 * @since 23- 06- 2020 
 * @version 1.8.0
 */

import java.util.Scanner;

public class LogIn {
	public static void main(String[]args) {
		
	}
		
	/**
	 * Here login and create account.
	 */
	public void Option() {
		System.out.println();
		System.out.println("1-LOGIN");
		System.out.println("2-CREATE ACCOUNT");
		System.out.println();
	}
	
	/**
	 * Check login and enter the username and password.
	 * 
	 * @param username
	 * @param password
	 */
	public void logInMain(String username, String password) {
		String validity = "check";
		System.out.println("");
		System.out.println("LOGIN");
		System.out.println("");
		do  {
			Scanner input = new Scanner(System.in);
			System.out.print("ENTER YOUR USER NAME : ");
			String inputusername = input.next();
			System.out.print("ENTER YOUR PASSWORD : ");
			String inputpassword = input.next();
			if (inputusername.equals(username) && inputpassword.equals(password)) {
				validity = "checked";
				System.out.println("");
				System.out.println(" !!!!!SUCESSFULLY ACCESS!!!!! ");
				System.out.println("!!WELCOME TO J/VADA HINDU GIRLS COLLEGE!!");
				System.out.println("");
				System.out.println("");
			} else {
				validity = "check";
				System.out.println("");
				System.out.println("!!Incorrect username or Password!!");
				System.out.println("");
				System.out.println("Try again");
				System.out.println("");
			}
		}while((validity == "check"));
	}

	/**
	 * Here you have create your account Teachers or student  can create their own account.
	 */
	public void creatAccount() {
		Scanner input = new Scanner(System.in);
		System.out.println("");
		System.out.println("CREATE ACCOUNT");
		System.out.println("");
		System.out.print("ENTER YOUR USER NAME : ");
		String username = input.next();
		System.out.print("ENTER YOUR PASSWORD : ");
		String password = input.next();
		System.out.println("");
		System.out.println("$$ ACCOUNT SUCESSFULLY CREATED $$");
		System.out.println("");
		System.out.println("WELCOME TO J/VADA HINDU GIRLS COLLEGE");
		System.out.println("");
		System.out.println("");
		LogIn main = new LogIn();
		main.logInMain(username, password);
	}
	
	/**
	 * Teacher login here.
	 */
	public void loginTeacher() {
		LogIn teacher = new LogIn();
		teacher.Option();
		Scanner input = new Scanner(System.in);
		System.out.print("Selection : ");
		int type = input.nextInt();
		if (type == 1) {
			String username = "Sabashan";
			String password = "@123";
			teacher.logInMain(username, password);
			System.out.println("      !!HELLO TEACHER WELCOME!!     ");
		}
	}

	/**
	 * Student login here. 
	 */
	public void loginStudent() {
		LogIn student = new LogIn();
		student.Option();
		Scanner input = new Scanner(System.in);
		System.out.print("Selection\t: ");
		int type = input.nextInt();
		if (type == 1) {
			String username = "Kathusha";
			String password = "1999k";
			student.logInMain(username, password);
			System.out.println("      !!HELLO STUDENT WELCOME!!       ");
		}
		if (type == 2) {
			student.creatAccount();
			System.out.println("      !!HELLO STUDENT WELCOME!!       ");
		}
	}
}