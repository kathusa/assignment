package Bcas.Pro.As;

/**
 * To the main system of the report card.
 * 
 * @author KathuSha
 * @since 23- 06- 2020 
 * @version 1.8.0
 * 
 */
 
import java.util.Scanner;

public class Main {
	public static void main(String args[]) {
		StudentRecordSystem select = new StudentRecordSystem();
		LogIn demo = new LogIn();
		System.out.println("");
		System.out.println(" !!----------J/VADA HINDU GIRLS COLLEGE----------!!");
		System.out.println("             !!-----REPORT CARD----- !! ");
		System.out.println("                 !!-----2020----- !! ");
		System.out.println();
		System.out.println();
		System.out.println("1-TEACHER");
		System.out.println("2-STUDENT");
		System.out.println("");
		Scanner input = new Scanner(System.in);
		System.out.print("Selection : ");
		int n = input.nextInt();
		System.out.println();
		if (n == 1) {
			/**
			 * To selection here. 
			 */
			demo.loginTeacher();
			System.out.println();
			System.out.println("1-ADD NEW STUDENT DETAILS");
			System.out.println("2-VIEW OLD STUDENT DETAILS");
			System.out.println("");
			Scanner input2 = new Scanner(System.in);
			System.out.print("Selection: ");
			int k = input2.nextInt();
			System.out.println();
			
			/**
			 * Teacher details input here.
			 */
			if (k == 1) {
				select.teacherInputDetails();
			}if (k == 2) {
				select.teacher();
			}
			
		}
		if (n == 2) {
			demo.loginStudent();
			select.student();
		}
	}
}