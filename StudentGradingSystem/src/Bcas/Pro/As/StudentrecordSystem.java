package Bcas.Pro.As;

/**
 * To student details record system
 * All of calculations and grading ranking system maintain from here
 * @author KathuSha
 * @since 23- 06- 2020 
 * @version 1.8.0
 */
import java.util.Scanner;

public class StudentRecordSystem {
	public void teacher() {
		StudentRecordSystem select = new StudentRecordSystem();
		System.out.println("");
		System.out.println("YOU CAN SEE THE STUDENT DETAILS HERE");
		System.out.println("");
		System.out.println("1-ALL STUDENT DETAILS");
		System.out.println("2-SPECIFIC STUDENT DETAILS ");
		System.out.println("3-HIGHER RANKER DETAILS IN EACH SUBJECT");
		System.out.println("");
		Scanner input = new Scanner(System.in);
		System.out.print("Selection : ");
		int n = input.nextInt();
		System.out.println();
		if (n == 1) {
			select.oldDetails(n);
		}
		if (n == 2) {
			select.oldDetails(n);
		}
		if (n == 3) {
			select.oldDetails(n);
		}
	}
	/**
	 * you can view student details here
	 */
	public void student() {
		StudentRecordSystem select = new StudentRecordSystem();
		System.out.println();
		System.out.println("YOU CAN VIEW YOUR REPORT HERE");
		System.out.println();
		select.oldDetails(2);
	}

	public void teacherInputDetails() {
		/**
		 * You can add students details here
		 */
		StudentRecordSystem select = new StudentRecordSystem();
		Scanner input = new Scanner(System.in);
		System.out.println();
		System.out.print("HOW MANY STUDENTS YOU WANT TO ADD  : ");
		int inputNo = input.nextInt();
		System.out.println();
		int[] indexNo = new int[inputNo];
		String[] studentName = new String[inputNo];
		int grade = 0;
		int[] maths = new int[inputNo];
		int[] science = new int[inputNo];
		int[] tamil = new int[inputNo];
		int[] english = new int[inputNo];
		for (int i = 0; i < inputNo; i++) {
			Scanner input1 = new Scanner(System.in);
			System.out.print("INDEX NUMBER : ");
			indexNo[i] = input1.nextInt();
			Scanner input2 = new Scanner(System.in);
			System.out.print("ENTER YOUR NAME : ");
			studentName[i] = input2.nextLine();
			Scanner input3 = new Scanner(System.in);
			System.out.print("ENTER YOUR GRADE : ");
			grade = input3.nextInt();
			Scanner input4 = new Scanner(System.in);
			System.out.print("ENTER YOUR MARKS IN MATHS : ");
			System.out.print("ENTER YOUR MARKS IN SCIENCE : ");
			System.out.print("ENTER YOUR MARKS IN TAMIL : ");
			System.out.print("ENTER YOUR MARKS IN ENGLISH : ");	
			System.out.println();
			maths[i] = input4.nextInt();
			science[i] = input4.nextInt();
			tamil[i] = input4.nextInt();
			english[i] = input4.nextInt();
		}
		/**
		 * you can the report and some selection here.Teachers can add student details. And also, they can view the student details.  
		 */
		System.out.println("");
		System.out.println("NOW YOU CAN GET THE REPORTS");
		System.out.println("");
		System.out.println("1-ALL STUDENT DETAILS");
		System.out.println("2-SPECIFIC STUDENT DETAILS ");
		System.out.println("3-HIGHER RANKER DETAILS IN EACH SUBJECT");
		System.out.println("");
		Scanner input3 = new Scanner(System.in);
		System.out.print("Selection\t: ");
		int y = input3.nextInt();
		System.out.println();
		if (y == 1) {
			select.rankedList(indexNo, studentName, grade, maths, science, tamil, english, 1);
		}
		if (y == 2) {
			select.rankedList(indexNo, studentName, grade, maths, science, tamil, english, 2);
		}
		if (y == 3) {
			select.rankedList(indexNo, studentName, grade, maths, science, tamil, english, 3);
		}
	}
	/**
	 * The student record here 
	 * @param y
	 */
	public void oldDetails(int y) {
		StudentRecordSystem select = new StudentRecordSystem();
		int[] indexNo = { 8001, 8002, 8003, 8004, 8005 };
		String[] studentName = { "K.Kathusha", "P.Sukanya", "V.Dilany", "T.Kamsa", "V.Vibisa" };
		int grade = 8;
		int[] maths = { 81, 72, 65, 92, 88 };
		int[] science = { 71, 31, 41, 12, 91 };
		int[] tamil = { 95, 42, 71, 71, 62 };
		int[] english = { 92, 74, 61, 81, 52 };
		if (y == 1) {
			
			select.rankedList(indexNo, studentName, grade, maths, science, tamil, english, 1);
		}
		if (y == 2) {
			select.rankedList(indexNo, studentName, grade, maths, science, tamil, english, 2);
		}
		if (y == 3) {
			select.rankedList(indexNo, studentName, grade, maths, science, tamil, english, 3);
		}
	}
	/**
	 * Teachers can only access for the class.
	 * 
	 * @param indexNo
	 * @param studentName
	 * @param grade
	 * @param maths
	 * @param science
	 * @param tamil
	 * @param english
	 * @param y
	 */
	public void rankedList(int[] indexNo, String[] studentName, int grade, int[] maths, int[] science, int[] tamil,
			int[] english, int y) {
		StudentRecordSystem select = new StudentRecordSystem();
		int tot[] = new int[indexNo.length];
		int temp;
		int sID;
		String sName;
		int sMaths;
		int sScience;
		int sTamil;
		int sEnglish;
		for (int i = 0; i < indexNo.length; i++) {
			int total = maths[i] + science[i] + tamil[i] + english[i];
			tot[i] = total;
		}
		for (int j = 0; j < indexNo.length; j++) {
			for (int k = j + 1; k < indexNo.length; k++) {
				if (tot[j] < tot[k]) {
					temp = tot[j];
					tot[j] = tot[k];
					tot[k] = temp;

					sID = indexNo[j];
					indexNo[j] = indexNo[k];
					indexNo[k] = sID;

					sName = studentName[j];
					studentName[j] = studentName[k];
					studentName[k] = sName;

					sMaths = maths[j];
					maths[j] = maths[k];
					maths[k] = sMaths;

					sScience = science[j];
					science[j] = science[k];
					science[k] = sScience;

					sTamil = tamil[j];
					tamil[j] = tamil[k];
					tamil[k] = sTamil;

					sEnglish = english[j];
					english[j] = english[k];
					english[k] = sEnglish;
				}
			}
		}
		if (y == 1) {
			/**
			 *  All student reports here
			 */
			for (int n = 0; n < indexNo.length; n++) {
				System.out.println();
				System.out.println("RANK :" + n + 1);
				System.out.println();
				select.details(indexNo[n], studentName[n], maths[n], science[n], tamil[n], english[n]);
			}
		}
		if (y == 2) {
			/**
			 * specific student report here
			 */
			int correction = 1;
			do {
				System.out.println();
				Scanner input = new Scanner(System.in);
				System.out.print("Index No : ");
				int sINo = input.nextInt();
				System.out.println();
				for (int i = 0; i < indexNo.length; i++) {
					int stINo = indexNo[i];
					if (sINo == stINo) {
						System.out.println();
						System.out.println("RANK :" + i + 1);
						System.out.println();
						select.details(indexNo[i], studentName[i], maths[i], science[i], tamil[i], english[i]);
						correction = 2;
						break;
					} else {
						if (i == indexNo.length - 1) {
							System.out.println();
							System.out.println("!!Invalid IndexNo!!");
							System.out.println();
						}
					}
				}
			} while (correction == 1);
		}
		if (y == 3) {
			/** 
			 *here  higher ranker holder  in each subject
			 */
			int correction = 1;
			do {
				Scanner input = new Scanner(System.in);
				System.out.println("Which subject's higher rank holder do you want to get?");
				System.out.println("1-Maths");
				System.out.println("2-Science");
				System.out.println("3-Tamil");
				System.out.println("4-English");
				System.out.println();
				System.out.print("Selection\t:");
				int typeForSubject = input.nextInt();
				System.out.println();
				System.out.println();
				if (typeForSubject == 1) {
					String subjectName = "MATHS";
					select.topperInEachSubjects(indexNo, studentName, grade, maths, subjectName);
					break;
				} else if (typeForSubject == 2) {
					String subjectName = "SCIENCE";
					select.topperInEachSubjects(indexNo, studentName, grade, maths, subjectName);
					break;
				} else if (typeForSubject == 3) {
					String subjectName = "TAMIL";
					select.topperInEachSubjects(indexNo, studentName, grade, maths, subjectName);
					break;
				} else if (typeForSubject == 4) {
					String subjectName = "ENGLISH";
					select.topperInEachSubjects(indexNo, studentName, grade, maths, subjectName);
					break;
				} else {
					correction = 1;
					System.out.println();
					System.out.println("!!Invalid IndexNo!!");
					System.out.println();
				}
			} while (correction == 1);
		}
	}
	/**
	 * 
	 * @param indexNo
	 * @param studentName
	 * @param grade
	 * @param mark
	 * @param subjectName
	 */
	public void topperInEachSubjects(int[] indexNo, String[] studentName, int grade, int[] mark, String subjectName) {
		int maxMark = 0;
		int maxindexNo = 0;
		String maxStudentName = null;
		for (int i = 0; i < mark.length; i++) {
			if (mark[i] > maxMark) {
				maxMark = mark[i];
				maxindexNo = indexNo[i];
				maxStudentName = studentName[i];
			}
		}
		/**
		 * Ranking of students marks 
		 */
		GardingRanking grad = new GardingRanking();
		System.out.println("         ======================================");
		System.out.println("                TOP RANKED STUDENT IN " + subjectName);
		System.out.println("           ===============================");
		System.out.println("               Index No  : " + maxindexNo);
		System.out.println("               Name      : " + maxStudentName);
		System.out.println("               Grade     : " + grade);
		System.out.println("               Marks     : " + maxMark);
		grad.gradingRanking(maxMark);
		System.out.println("           ================================");
	}
	/**
	 * This is the final report card print get the all deatatils here
	 * @param studentIndexNumber
	 * @param studentName
	 * @param Maths
	 * @param Science
	 * @param Tamil
	 * @param English
	 */
	public void details(int studentIndexNumber, String studentName, int Maths, int Science, int Tamil, int English) {
		int total = Maths + Science + Tamil + English;
		int percent = total / 4;
		System.out.println("============================================================");
		System.out.println("    STUDENT INDEX NO :" + studentIndexNumber + "    STUDENT NAME :" + studentName);
		System.out.println("============================================================");
		System.out.println();
		System.out.println("SUBJECT |  MARKS");
		System.out.println();
		System.out.println("Maths : " + Maths);
		GardingRanking grad = new GardingRanking();
		grad.gradingRanking(Maths);
		System.out.println("Science : " + Science);
		grad.gradingRanking(Science);
		System.out.println("Tamil : " + Tamil);
		grad.gradingRanking(Tamil);
		System.out.println("English :" + English);
		grad.gradingRanking(English);
		System.out.println("-------------------------------------------------------------");
		System.out.println("||TOTAL MARKS|| : " + total + "/400");
		System.out.println("||PERCENTAGE||  : " + percent);
		System.out.println("-------------------------------------------------------------");
		System.out.println("");
		System.out.println("=============================================================");
	}
}



