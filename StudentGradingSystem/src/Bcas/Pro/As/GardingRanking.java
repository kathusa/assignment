package Bcas.Pro.As;

/**
 * To Grading Ranking system. 
 * 
 * @author KathuSha
 * @since 23- 06- 2020 
 * @version 1.8.0
 * 
 */

public class GardingRanking {
	public void gradingRanking(int marks) {
		if (marks >= 75) {
			System.out.println("GRADE A");
		} else if ((marks < 74) && (marks >= 65)) {
			System.out.println("GRADE B");
		} else if ((marks < 64) && (marks >= 55)) {
			System.out.println("GRADE C");
		} else if ((marks < 54) && (marks >= 35)) {
			System.out.println("GRADE D");
		} else {
			System.out.println("GRADE F");
		}
	}
}
