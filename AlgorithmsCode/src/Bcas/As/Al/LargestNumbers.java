package Bcas.As.Al;


import java.util.Scanner;

public class LargestNumbers {
	public static void main(String[]args) {
		int Num1 ;						
		int Num2 ;
		int Num3 ; 
		
		Scanner N = new Scanner (System.in);
		System.out.print("ENTER YOUR FIRST NUMBER");
		Num1 = N.nextInt();
		System.out.print("ENTER YOUR SECOND NUMBER");
		Num2 = N.nextInt();
		System.out.print("ENTER YOUR THIRD NUMBER");
		Num3 = N.nextInt();
		if (Num1>Num2 && Num1>Num3){	/*if conditions given in  are true, which means Num1 is greater than Num2 and Num3.
										 * Num1 is grater than Num2 but less than Num3.which means num3 is the largest 
										 *	That means Num1 is largest 	
										 */
		System.out.println("LARGEST NUMBER IS! :" + Num1); 
		}
		else if(Num2>Num3) {			/*if the condition in outer if is false  and inner if is true which means 
										*num3 is greater than Num1 but Num2 is greater than Num3. That means Num2 is largest
										*/
			System.out.println("LARGEST NUMBER IS! :" + Num2);
		}
		else {							/*if the condition in outer if is false and inner if is false
		 								 * which means Num3 is greater than Num1 and Num2. That means Num3 is largest
										 */
			System.out.println("LARGEST NUMBER IS! :" + Num3);
		}
	}
}


/* OUTPUT /*-
 * 
ENTER YOUR FIRST NUMBER 45
ENTER YOUR SECOND NUMBER 20
ENTER YOUR THIRD NUMBER 50
LARGEST NUMBER IS! :50
 */



