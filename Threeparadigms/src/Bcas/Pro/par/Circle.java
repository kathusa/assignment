package Bcas.Pro.par;

public class Circle {

    private int radius; 
    /*It is the radius of the circle
     */
    
    public void setRadius(int radius) {
        this.radius = radius;
        /*This is the setRadius() method. The this variable is a special variable which 
         * we use to access the member fields from methods.
         */
    }

    public double area() {
        
        return this.radius * this.radius * Math.PI;
        /*The area() method returns the area of a circle. The Math.PI is a built-in constant.
         */
    }


    public static void main(String[] args) {
        
        Circle c = new Circle();
        c.setRadius(5);     
        /* We create an instance of the Circle class and set its radius by calling the 
         * setRadius() method on the object of the circle
         */

        System.out.println("Area of circle : "+c.area());
    }
}

/* OUTPUT /*
 * Area of circle : 78.53981633974483
 */
